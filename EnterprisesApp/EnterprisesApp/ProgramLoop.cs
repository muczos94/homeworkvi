﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using EnterprisesApp.ConsoleHelper;

namespace EnterprisesApp
{
    public class ProgramLoop
    {
        public const string FilePathConst =
            @"C:\Users\muczo\Desktop\Homework\homework_vi\CompaniesRevenies\CompaniesRevenies_";

        public static Dictionary<string, CompaniesData> CompaniesDatas = new Dictionary<string, CompaniesData>();
        public static string CompanyName;
        public static Task<int> Task1;
        public static bool Async;
        public static DateTime DateSince;
        public static DateTime DateTo;

        public enum CommandTypes
        {
            Test,
            AllRevenuesSynch,
            AllRevenuesAsynch,
            RevenuesCompany,
            Exit
        }

        public static void Execute()
        {
            while (true)
            {
                var command = ConsoleReadHelper.GetCommandTypes();

                switch (command)
                {
                    case CommandTypes.Test:
                        CompaniesDatas.Clear();
                        Console.WriteLine("Niewlasciwa komenda");
                        break;

                    case CommandTypes.AllRevenuesSynch:
                        Async = false;
                        CompaniesDatas.Clear();
                        CalculateRevenuesFromAllFiles();
                        break;

                    case CommandTypes.AllRevenuesAsynch:
                        CompaniesDatas.Clear();
                        Async = true;
                        CalculateRevenuesFromAllFiles();
                        break;

                    case CommandTypes.RevenuesCompany:
                        CompaniesDatas.Clear();
                        RevenuesCompany();
                        break;
                    case CommandTypes.Exit:
                        return;
                    default:
                        Console.WriteLine("nie tedy droga");
                        break;
                }
            }
        }

        private static void CalculateRevenuesFromAllFiles()
        {
            string filePath;
            var jsonHelper = new JsonHelper();
            var consoleWriteHelper = new ConsoleWriteHelper();

            for (var i = 1970; i <= 2017; i++)
            {
                filePath = FilePathConst + Convert.ToString(i);
                var file = jsonHelper.LoadJson(filePath); //laduje plik json z konkretna data 
                if (!Async)
                    CalculateAll(file); //sekwencyjnie
                else
                    Task1 = Task.Run(() => CalculateAll(file)); //asynchronicznie
            }
            Task.WaitAll();
            consoleWriteHelper.WriteAllRevenues(CompaniesDatas);

            Console.WriteLine("skonczone, można raport wypisac");
        } // zliczanie przychodow sekwencyjnie oraz asynchronicznie

        private static void RevenuesCompany()
        {
            SelectCompany();
            GetProperDate();
            var date2JustYear = ConsoleReadHelper.SplitYear(DateTo);

            var CompanyObject = new CompaniesData();
            CompanyObject.Name = "Company " + CompanyName;
            var jsonHelper = new JsonHelper();
            for (var date1JustYear = ConsoleReadHelper.SplitYear(DateSince);
                date1JustYear <= date2JustYear;
                date1JustYear++)
            {
                var list = jsonHelper.LoadJson(FilePathConst + date1JustYear);

                foreach (var company in list)
                    if (company.Name.Equals(CompanyObject.Name))
                        if (company.Date > DateSince)
                            if (company.Date < DateTo)
                                CompanyObject.Value += company.Value;
            }

            var consoleWrite = new ConsoleWriteHelper();
            consoleWrite.WriteRevenuesForCompany(CompanyObject, DateSince, DateTo);
        } // zliczanie przychodow dla pojedynczej firmy w danym okresie

        private static void GetProperDate()
        {
            do
            {
                Console.Write("Podaj date nr 1 [yyyy]-[mm]-[dd]:");
                DateSince = ConsoleReadHelper.GetDate(Console.ReadLine()); // podaj date pierwsza
                Console.Write("Podaj date nr 2 [yyyy]-[mm]-[dd]:");
                DateTo = ConsoleReadHelper.GetDate(Console.ReadLine()); // podaj date druga
            } while (DateSince > DateTo);
        }

        private static int CalculateAll(List<CompaniesData> file)
        {
            foreach (var company in file)
                if (CompaniesDatas.ContainsKey(company.Name))
                    CompaniesDatas[company.Name].Value += company.Value;
                else
                    CompaniesDatas.Add(company.Name, company);
            return 0;
        } // uzupelnianie danych 

        private static void SelectCompany()
        {
            Console.Write("Podaj nazwe firmy dla ktorej chcesz uzyskac przychody:\n" +
                          "Dostepne nazwy: B, E, L, K, I, O, G, D, J, N, F, H, M, C, A: ");
            CompanyName = ConsoleReadHelper.GetCompanyName(Console.ReadLine());
            Console.WriteLine("Nazwa firmy: " + "Company " + CompanyName);
        } // wybranie firmy
    }
}
