﻿using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;

namespace EnterprisesApp
{
    internal class JsonHelper
    {
        public List<CompaniesData> LoadJson(string filePath)
        {
            using (var File = new StreamReader(filePath + ".json"))
            {
                var json = File.ReadToEnd();
                var jsonFile = JsonConvert.DeserializeObject<List<CompaniesData>>(json);
                return jsonFile;
            }
        } //json deserialize
    }
}
