﻿using System;
using System.Text.RegularExpressions;

namespace EnterprisesApp.ConsoleHelper
{
    public class ConsoleReadHelper
    {
        public static ProgramLoop.CommandTypes GetCommandTypes()
        {
            ProgramLoop.CommandTypes commandType;
            Console.WriteLine("\n\n1. AllRevenuesSynch,\n" +
                              "2. AllRevenuesAsynch,\n" +
                              "3. RevenuesCompany,\n" +
                              "4. Exit\n");

            while (!Enum.TryParse(Console.ReadLine(), out commandType))
                Console.WriteLine("nieprawidłowa wpisana komenda, wpisz jeszcze raz: ");

            return commandType;
        }

        public static DateTime GetDate(string input)
        {
            var dateStart = new Regex(@"^([1-2]\d{3})-([0&&1]\d)-([0-3]\d)$");

            while (!dateStart.IsMatch(input) || string.IsNullOrEmpty(input))
            {
                Console.Write("Podaj poprawna date: ");
                input = Console.ReadLine();
            }
            var date = DateTime.Parse(input);

            return date;
        }

        public static int SplitYear(DateTime year)
        {
            var a = year.ToString().Substring(6, 4);

            var x = int.Parse(a);
            return x;
        }

        public static string GetCompanyName(string input)
        {
            var companyName = new Regex(@"^[B, E, L, K, I, O, G,D,J,N,F,H,M,C,A]$");
            while (!companyName.IsMatch(input))
            {
                Console.WriteLine("Podaj nazwe firmy, ktora istnieje w bazie: ");
                input = Console.ReadLine();
            }

            return input;
        }
    }
}
