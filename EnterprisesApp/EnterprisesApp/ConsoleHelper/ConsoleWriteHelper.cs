﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EnterprisesApp.ConsoleHelper
{
    public class ConsoleWriteHelper
    {
        public void WriteAllRevenues(Dictionary<string, CompaniesData> companiesDatas)
        {
            foreach (var company in companiesDatas)
                Console.WriteLine("Nazwa firmy: " + "'" + company.Value.Name + "'" + "---" + " Przychod: " +
                                  company.Value.Value + " zl");
        } //wypisanie przychodu

        public void WriteRevenuesForCompany(CompaniesData companyObject, DateTime dateSince, DateTime dateTo )
        {
            if (companyObject.Value == 0)
            {
                Console.WriteLine("Podales niewlasciwy zakres, albo firma nie istnieje");
            }
            else
            {
                Console.WriteLine("Firma: " + companyObject.Name + " uzyskala " + companyObject.Value +
                                  " zl dochodu w latach " + dateSince + " do " + dateTo);
            }
        }
    }


}
